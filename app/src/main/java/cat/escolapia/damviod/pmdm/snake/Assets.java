package cat.escolapia.damviod.pmdm.snake;

import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Sound;

public class Assets {
    public static Pixmap background;
    public static Pixmap logo;
    public static Pixmap mainMenu;
    public static Pixmap C_Joan;
    public static Pixmap buttons;
    public static Pixmap numbers;
    public static Pixmap ready;
    public static Pixmap pause;
    public static Pixmap gameOver;
    public static Pixmap headUp;
    public static Pixmap headLeft;
    public static Pixmap headDown;
    public static Pixmap headRight;
    public static Pixmap tail;
    public static Pixmap diamond;
    public static Pixmap Wall;
    public static Pixmap bg1;
    public static Pixmap bg2;
    public static Pixmap bg3;
    public static Pixmap bg4;
    public static Sound click;
    public static Sound eat;
    public static Sound xoc;
}
