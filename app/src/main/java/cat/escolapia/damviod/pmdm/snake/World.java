package cat.escolapia.damviod.pmdm.snake;

import java.util.Random;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;

public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.01f;
    public enum Backgrounds {bg1,bg2,bg3,bg4,bg5}
    static public Backgrounds actual_bg;
    static public Backgrounds anterior_bg;
    public Snake snake;

    public static Wall[] walls;

    public static Diamond[] diamonds = new Diamond[5];
    public static int num_dia;
    public boolean gameOver = false;
    static public int score = 0;

    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;

    public World() {
        num_dia = 1;
        anterior_bg = Backgrounds.bg1;
        actual_bg = anterior_bg;
        snake = new Snake();
        placeDiamond();
        generateWalls(3);
    }

    public void placeDiamond() {
        generateDia();

        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }

        int len = snake.parts.size();
        for (int i = 0; i < len; i++) {
            SnakePart part = snake.parts.get(i);
            fields[part.x][part.y] = true;
        }



    }


    public void generateWalls(int x)
    {
        walls = new Wall[x];
        for(int i = 0; i < x; i++)
        {
            int _x = i;
            int _y = i;
            walls[i] = new Wall(_x,_y);

        }
    }

    public final void generateDia()
    {
        for(int i = 0; i < num_dia ;i++)
        {
            int diamondX = random.nextInt(WORLD_WIDTH);
            int diamondY = random.nextInt(WORLD_HEIGHT);
            while (true) {
                if (fields[diamondX][diamondY] == false) break;
                diamondX += 1;
                if (diamondX >= WORLD_WIDTH) {
                    diamondX = 0;
                    diamondY += 1;
                    if (diamondY >= WORLD_HEIGHT) {
                        diamondY = 0;
                    }
                }

            }
            if(walls != null)
            {
                for(int j = 0; j < walls.length; j++)
                {
                    if(diamondX == walls[j].x && diamondY == walls[j].y)
                    {
                        diamondX = i+1;
                        diamondY = i+1;
                    }
                }
            }
            diamonds[i] = new Diamond(diamondX, diamondY, Diamond.TYPE_1);
            diamonds[i].can_be_rendered = true;
            GameScreen.actual_dia++;
        }

    }

    public void update(float deltaTime) {
        if (gameOver) return;
        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            System.out.println(GameScreen.actual_dia);
            snake.advance();
            if (snake.checkXoca()) {
                gameOver = true;
                return;
            }
            SnakePart head = snake.parts.get(0);
            //Comprovació x i y dels head igual a diamant.
            // Si sí, score = score + INCREMENTSCORE, allarga serp i cridar a placeDiamond de nou... adicionalment
            // es pot baixar el tick per augmentar la dificultat
            if(GameScreen.actual_dia == 0){placeDiamond();}
            else
            {
                for(int i = 0; i < GameScreen.actual_dia;i++) {
                    //System.out.println(World.diamonds[i].can_be_rendered);
                    if (World.diamonds[i] != null) {
                        if (head.x == diamonds[i].x && head.y == diamonds[i].y) {
                            tick = tick - TICK_DECREMENT;
                            score = score + SCORE_INCREMENT;
                            Snake.vel = Snake.vel + 1;
                            snake.allarga();
                            //diamonds[i].can_be_rendered = false;
                            GameScreen.actual_dia--;
                        }
                    }
                }
            }
            if(walls.length > 0)
            {
                int len = walls.length;
                for(int i = 0; i < len;i++)
                {
                    if(head.x == walls[i].x && head.y == walls[i].y)
                    {
                        gameOver = true;
                        return;
                    }
                }
            }
        }
    }
}
